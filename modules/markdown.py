import markdown

def run(params, variables, log):
	return markdown.markdown(open(params[0],'r').read(),extensions=['extra','codehilite'], output_format='html5')