import glob, os, markdown, glob
from datetime import datetime
import util

def run(params, variables, log):
	tpl = params[0]
	
	if params[1] == 'archive':
	
		blog_dir = params[2]
	
		d = {}
		archive = {}
		foo = ''
		for f in glob.glob(os.path.join(blog_dir,'*')):
			archive[f] = {}
			for line in open(f,'r').readlines():
				if line.startswith('%'):
					archive[f][line[1:].split('=')[0].strip()] = line[1:].split('=')[1].strip()
		
		years = {}
		for item in archive:
			str_d = archive[item]['post_date']
			y = datetime.strptime(str_d,'%m/%d/%Y')
			y1 = y.strftime('%Y')
			m = datetime.strptime(str_d,'%m/%d/%Y')
			m1 = m.strftime('%B')
			
			if y1 not in years.keys():
				years[y1] = {}
			
			if m1 not in years[y1].keys():
				years[y1][m1] = []
			
			years[y1][m1].append(archive[item]['post_title'])
		
		
		foo += '<ul class="years">\n'
		for year in years:
			
			foo += '<li><a title="%s" href="%s">%s</a></li>\n' % (year,'#',year)
			
			foo += '<ul class="months">\n'
			for month in years[year]:
				foo += '<li><a title="%s" href="%s">%s</a></li>\n' % (month,'#',month)
				if years[year][month]:
					foo += '<ul>\n'
					for p in years[year][month]:
						foo += '<li><a title="%s" href="%s">%s</a></li>\n' % (p,'/blog/p/'+p.lower().replace(' ','-')+'.html',p)
					foo += '</ul>\n'
			foo += '</ul>\n'
		
		foo += '</ul>\n'
		
		d['menu_title'] = 'archive'
		d['menu_content'] = foo
		
		return util.get_template(tpl,d)
		
	return ''