Quicksite is a static website generator. It is written in
[Python](http://python.org) and uses INI files and templates to generate
the HTML files.

For example:

The site.ini file contains:

```
:::ini
[output]
$foo=Hello
$bar=World

content=content.tpl
```

The 'content' option is not significant. However, the order that the .tpl options
appear is respected. For instance:

```
:::text
2=template2.tpl
1=template1.tpl
```

In the above example, 'template2.tpl' will be parsed before 'template1.tpl'.

The content.tpl file contains:

```
:::html
<html>
<body>
$foo $bar
</body>
</html>
```

If the section headers do not end with .html, then a directory is created
and index.html is generated.

Multiple section headers can be specified for multiple HTML file outputs:

```
:::ini
[output1]
$foo=Hello
content=content.tpl
[output1/test.html]
$foo=World
content=content.tpl
```
Variables in the INI files begin with the '$' character. Variables can 
point to plain text or function calls. 

'$' can be escaped with '$$'.

If variables are not specified, they will default to an empty string.

Global variables may also be specified with:

```
:::ini
[g:content.tpl]
$foo=Hello World

[output1]
content=content.tpl

[output2]
content=content.tpl
```

In the above example, '$foo' will be replaced with the globally specified 
variable for all instances of '$foo' for 'content.tpl'.

Function calls begin with the '^' character. The parameters are enclosed in
parentheses and separated with the '|' character.

For example:

```
:::ini
$foobar=^foobar(param1|param2|param3)
```

Typical function calls are:

  * ^file(file_location)
  * ^markdown(file_location)

The file function call will replace the variable in a template with the 
plain text file at 'file_location'.

The markdown function call will replace the variable in a template with the 
HTML markup of the Markdown file at 'file_location'.

Inline Python code may used in the templates.

For example:

```
<%
for x in range(0,10):
	print '%i: %s<br>' % x, '$foo'
%>
```

And:

```
This is a <% print 'line' %> of text.
```

### Usage

```
:::text
usage: quicksite.py [-h] [-s [PORT]] [-i [FILE [FILE ...]]] [-c DIRECTORY]
                    [-v]

Quicksite: Static Website Generator

optional arguments:
  -h, --help            show this help message and exit
  -s [PORT], --serve [PORT]
                        serve the site on local host (default: 8000)
  -i [FILE [FILE ...]], --input [FILE [FILE ...]]
                        site INI file (default: site.ini)
  -c DIRECTORY, --create DIRECTORY
                        create a site using the default template at DIRECTORY
  -v, --verbose         show all log messages
```

Site creation (--create, -c) is not functional yet. Site INI files can be
created by following the structure outlined above.

Websites are generated with:

```
:::text
python quicksite.py site1.ini site2.ini ...
```

Or if using the default 'site.ini' file name:

```
:::text
python quicksite.py
```

Websites can be tested after generation with:

```
:::text
python quicksite.py -s -i site.ini
```

Or if using the default 'site.ini' file name:

```
:::text
python quicksite.py -s
```

The default port is 8000, a different port can be used with:

```
:::text
python quicksite.py -s <port>
```

### Modules

Quicksite is easily extensible. The modules are read from the 'modules'
subdirectory. Each extension module file name is the name of the function
call.

The structure of the module files are as follows:

```
:::python
def run(params, variables, log):
	#do something with params
	return 'Hello World'
```

For instance, the Markdown module, markdown.py, contains:

```
:::python
import markdown

def run(params, variables, log):
	return markdown.markdown(open(params[0],'r').read(),extensions=['extra','codehilite'], output_format='html5')
```

The 'params' variable contains a dictionary of the parameters for the
function call, and the 'variables' argument contains a dictionary of the
template variables for the section the function is called from, including
globals. The 'log' argument contains the log object to print output for
status and debug messages.

The 'log' object has the following methods:

 * title
 * subtitle
 * error
 * status
 * warning
 
Each method takes a single string argument.
