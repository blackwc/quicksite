import os, glob, re
from HTMLParser import HTMLParser

class parser(HTMLParser):
	data = ''
	bodyFlag = False
	def handle_starttag(self, tag, attrs):
		if tag.lower() == 'body':
			self.bodyFlag = True
	def handle_data(self, data):
		if self.bodyFlag: self.data += data.strip() + ' '

def main():
	
	foo = 'var searchTerms = {\n'

	for fn in glob.glob(os.path.join('..','publish','blog','p','*')):
		f = open(fn,'r')
		d = f.read()
		f.close()
		p = parser()
		p.feed(d)
		bar = p.data.replace('\n','')
		bar = re.sub(' +',' ',bar)
		foo += '\t"'+os.path.basename(fn)+'": "'+bar.strip()+'",\n'
	foo = foo[:-2]
	foo += '\n}\n'
	print foo
	
main()
	