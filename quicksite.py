from ConfigParser import SafeConfigParser as conf
from string import Template
import os, glob, argparse, SimpleHTTPServer, SocketServer
from collections import OrderedDict

import modules

class quickLog:
	
	def __init__(self,flag):
		self.LOG_QUIET = flag

	def title(self, msg):
		if self.LOG_QUIET: return
		print '=============================================='
		print ' ' + msg
		print '=============================================='
	def subtitle(self, msg):
		if self.LOG_QUIET: return
		print '----------------------------------------------'
		print  ' ' + msg
		print '----------------------------------------------'
	def status(self, msg):
		if self.LOG_QUIET: return
		print '>>', msg
	def error(self, msg):
		print 'ERROR:', msg
		exit()
	def warning(self, msg):
		if self.LOG_QUIET: return
		print 'WARNING:', msg

def generate(args, log):

	log.title('Extensions')
	mod = []
	for f in glob.glob(os.path.join('modules','*.py')):
		if f == os.path.join('modules','__init__.py'): continue
		op = os.path.splitext(os.path.basename(f))[0]
		mod.append(op)
		log.status('%s=modules.%s'%(op,op))

	if type(args) == str:
		args = [args]
	for arg in args:
	
		log.title(arg)
		
		site = conf()
		site.read(arg)
		
		foo = OrderedDict()
		d = OrderedDict()
		globals = OrderedDict()
		
		for section in site.sections():
			if section.startswith('g:'):
				globals[section[2:]] = OrderedDict()
				for option in site.options(section):
					val = site.get(section,option).encode('string-escape')
					globals[section[2:]][option[1:]] = val
					log.status('Processing: %s global %s'% (section[2:],option))
		
		for section in site.sections():
			if section.startswith('g:'): continue
			bar = OrderedDict()
			tees = OrderedDict()
			for option in site.options(section):

				val = site.get(section,option)
				
				if option.startswith('$'):
					tees[option[1:]] = val
					continue
				
				bar[option] = val
			
			#foo contains the templates
			#d contains the variables
			foo[section] = bar
			d[section] = tees
		
		d[';globals'] = globals
		
		modules.parse_calls(foo,d,mod,log)
			
def serve(port, site):

	c = ConfigParser()
	c.read(site)
	i=0
	s = c.sections()[i]
	while s.startswith('g:'):
		s = c.sections()[i]
		i+=1
	os.chdir(s)
	Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
	httpd = SocketServer.TCPServer(('',port), Handler)
	httpd.serve_forever()

def main():

	parser = argparse.ArgumentParser(description='Quicksite: Static Website Generator',
	argument_default=argparse.SUPPRESS)

	parser.add_argument('-s', '--serve', metavar='PORT', nargs='?', type=int
	, help='serve the site on local host (default: 8000)')
	parser.add_argument('-i', '--input', metavar='FILE', nargs='*',
	default='site.ini', help='site INI file (default: site.ini)')
	parser.add_argument('-c', '--create', metavar='DIRECTORY',
	nargs=1, help='create a site using the default template at DIRECTORY')
	parser.add_argument('-v', '--verbose', action='store_true',
	help='show all log messages')
	
	args = parser.parse_args()
	
	log_flag = True
	if hasattr(args, 'verbose'):
		log_flag = False
	log = quickLog(log_flag)
	
	if hasattr(args,'create'):
		log.error('Not functional yet.')
		return
		
	if hasattr(args,'serve'):
		port = 8000
		if args.serve != None:
			port = args.serve
		serve(port, args.input)
		return
	
	if hasattr(args,'input') and not hasattr(args,'serve'):
		generate(args.input, log)
		return
	
main()